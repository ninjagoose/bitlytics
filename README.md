# Bitlytics.co

Cryptocurrency analytics tool for the masses.

### Running Locally

```sh
$ git clone git@bitbucket.org:ninjagoose/bitlytics.git
$ cd bitlytics
$ npm install
$ node server.js
```

When the server is running, launch localhost:5000 in your favorite Webkit based browser.  booya.
