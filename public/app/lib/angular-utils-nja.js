angular.module( 'utils.nja', [])
.directive('preventDefault', function() {
	return function(scope,el,attrs) {
		var evt = attrs.preventDefault || 'click';
		
		el.bind(evt, function(e) {
			e.preventDefault();
		});
	};
})
.directive('stopPropagation', function() {
	return function(scope,el,attrs) {
		var evt = attrs.stopPropagation || 'click';

		el.bind(evt, function(e) {
			e.stopPropagation();
		});
	};
})
.directive('enterKeyup', function() {
	return function(scope,el,attrs) {
		el.bind("keyup", function(e) {
			if(e.which===13) {
				scope.$apply(attrs.enterKeyup);
			}
		});
	};
})
.directive('comingSoon', function() {
	return function(scope,el,attrs) {
		var evt = attrs.comingSoon || 'click';
		
		el.bind(evt, function(e) {
			e.preventDefault();e.stopPropagation();
			alert('coming soon');
		});
	};
})
.filter('isEmpty', function() {
	return function(input) {
		return _.isEmpty(input);
	};
})
.filter('ifElse', function() {
	return function(input,_if,_else) {
		return input ? _if : _else;
	};
})
.filter('toDate', function() {
	return function(input) {
		return new Date(input);
	};
})
.controller('DefaultCtrl', [ 
	'$scope',
	function($scope) {

	}
])

;