angular.module( 'bitlytics', [ 'ngRoute', 'utils.nja', 'angular-lodash' ])
.config([ '$routeProvider', '$httpProvider', '$locationProvider',
	function ($routeProvider, $httpProvider, $locationProvider) {

		// So far only default contollers (empty, in utils.nja)
		$routeProvider
			.when('/', { 
				templateUrl: '/app/views/home.html',
				controller: 'DefaultCtrl'
			})
			.when('/how', {
				templateUrl: '/app/views/how.html',
				controller: 'DefaultCtrl'
			})
			.when('/contact', {
				templateUrl: '/app/views/contact.html',
				controller: 'DefaultCtrl'
			})
			.otherwise({ redirectTo: '/' });

		// setup headers for ajax requests
		// should maybe add another header for the api?
		$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
		// setup !# so we can use pre-render to do SEO type shit
		$locationProvider.hashPrefix('!');
	}
])
.directive('highchartStockchart', function () {
	return {
		scope: {
			chartData: "=",
			width: "@",
			height: "@",
			title: "@"
		},
		link: function ($scope, $el, $attrs) {
			console.log('linking highchart', $scope.chartData);

			var chartsDefaults = {
				renderTo: $el[0],
				height: $attrs.height || null,
				width: $attrs.width || null
			};
			
			// Update when charts data changes
			$scope.$watch(function() { return $scope.chartData; }, function(next) {
				console.log('what is next for highchart?', next);
				if(!next) {
					return;
				}

				var c_opts = { width: $scope.width, height: $scope.height },
					c = {
						chart: angular.extend({}, chartsDefaults, c_opts), 
						series: [{
							name : 'Coinbase',
							data : 	next.reverse(),
							tooltip: {
								valueDecimals: 2
							}
						}],
						title : {
							text : $scope.title
						},
						rangeSelector : {
							selected : 1
						}
					};
				
				
				console.log('creating new stockchart', c, Highcharts);
				new Highcharts.StockChart(c);
			});
		}
	};
})
.directive('configFormPopover', function($compile,$http,$document) {
	return {
		link: function($scope,$el,$attrs) {
			$http({ method: 'GET', url: $attrs.template }).then(function(res) {
				$el.popover({ 
					html : true,
					placement: $attrs.placement,
					title: $attrs.title,
					content: res.data
				});
				$el.addClass('popoverBtn');
				$el.on('shown.bs.popover', function() {
					var pop = $('.popover-content');
					
					$compile(pop.contents())($scope);
					
					// pop.on('change', function() {
					// 	$el.popover('destroy');
					// });
					
					$document.on('click', function(e) {
						if(!$(e.toElement).closest('.popover-content')[0] && !$(e.toElement).closest('.popoverBtn')[0]) {
							$el.popover('hide');
						}
					});

					if(!$scope.$$phase) {
						$scope.$digest();
					}
				});
			}, function(res) {
				console.log('error with config button')
			});
		}
	};
})
.directive('tooltip', function($timeout) {
	return {
		link: function($scope,$el,$attrs) {
			$el.tooltip({
				placement: $attrs.placement,
				title: $attrs.tooltip
			});
			$el.on('click', function() {
				$el.tooltip('hide');
			});
		}
	};
})
.directive('loadingEllipsis', function($timeout) {
	return {
		scope: { loadingEllipsis: '=' },
		link: function($scope,$el,$attrs) {
			var count = $attrs.ellipsisCount || 3,
				up = 0,
				delay = $attrs.ellipsisDelay || 500,
				loading = function() {
					if(!$scope.loadingEllipsis) {
						if(up<=count) {
							$el[0].innerHTML += '.';
							up++;
						} else {
							$el[0].innerHTML = $el[0].innerHTML.slice(0, -(up-1));
							up=0;
						}
						$timeout(loading,delay);
					}
				};

			loading();
		}
	};
})
.filter('convertToPref', function() {
	return function(input,pref) {
		return input * (pref.fromUSD || 1);
	}
})
.service('appdata', function() {
	return {
		markets: {},
		currencies: [
			{
				name: 'Dollars',
				label: '$USD',
				id: 'usd',
				symbol: '$',
				sympos: 'pre',
				decimal: '.',
				thous: ','
			},
			{
				name: 'Euros',
				label: '€EUR',
				id: 'eur',
				symbol: '€',
				sympos: 'suf',
				decimal: ',',
				thous: '.',
				fromUSD: 0.74  // Need to get this from API
			}
		]
	};
})
.controller( 'AppCtrl', [ 
	'$scope', '$rootScope', '$timeout', '$http', '$q', 'appdata',
	function ( $scope, $rootScope, $timeout, $http, $q, appdata ) {
		console.log('initializing bitlytics');

		$scope.appdata = appdata;
		$scope.pref = {
			currency: $scope.appdata.currencies[0]
		};

		// Do query for USD/BTC data, right here;
		$http({
			method:'GET',
			url: '/api/v1/markets/btc_usd/price?q=bitstamp,btce,mtgox,coinbase'
		}).then(function(res) {
			angular.extend($scope.appdata.markets,res.data);
			
			if(!$scope.$$phase) {
				$scope.$digest();
			}
		}, function(err) {
			console.log('uh oh, looks like we can an error', err);
		});

		// Pull in coinbase historical data
		$http({
			method:'GET',
			url: '/api/v1/markets/btc_usd/history'
		}).then(function(res) {
			if(!$scope.appdata.markets.coinbase) {
				$scope.appdata.markets.coinbase = {};
			}

			$scope.appdata.markets.coinbase.history = res.data;
			
			if(!$scope.$$phase) {
				$scope.$digest();
			}
		}, function(err) {
			console.log('uh oh, looks like we can an error', err);
		});
	}
])

;
