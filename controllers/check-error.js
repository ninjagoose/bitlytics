// Checkk error module

var _ = require('lodash-node');
module.exports = function() {

	return function(req,res,next) {
		var err = res.locals.error;

		if(!err) {
			return next();
		}

		console.log(err);
		
		if(req.xhr) {
			res.json({ err: err });
		} else {
			res.send(err);
		}
	};
};
