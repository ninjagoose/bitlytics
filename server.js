// Boilerplate Express Web Server

console.log('Begin web server start up...');

// Load main web application server modules
var express = require('express'),
	app = express(),
	port = process.env.PORT || 5000,
	
	// Load utility modules
	http = require('http'),
	_ = require('lodash-node'),
	CryptoApi = require('./lib/crypto-api'),
	Q = require('q');

// Express Configuration and Middleware Stack
app.configure(function(){
	// Cookies
	app.use(express.cookieParser());

	// Prerender.io service for serving up content for search engines
	// app.use(require('prerender-node'));

	// Static serving
	app.use(express.static(__dirname + '/public'));
	app.use(express.favicon(__dirname + '/public/assets/favicon.ico'));

	// Parse request body
	app.use(express.bodyParser());

	// App server router
	app.use(app.router);

	// 404 serving
	app.use(function(req,res,next) {
		res.status(404).send('404 - Not Found');
	});

	// Error serving
	app.use(function(err, req, res, next){
		console.error(err.stack);
		res.status(500).send('500 - Unknown Server Error, terribly sorry');
	});
});

// Set API routes
app
.get('/api/v1/markets/:mkt/price/?', function(req,res,next) {
	var mkt = req.params.mkt,
		q = req.query.q.split(','),
		logAndCry = function(err) {
			console.log('ooooh noooo, an error in our api!!!!', err);
			res.status(500).send('we encountered an error, we will do all we can');
		};

	if(q) {
		if(q.length===1) {
			CryptoApi.getMarketPrice(mkt,q[0]).then(function(obj) {
				var result = {};
				result[q[0]] = obj;
				res.json(result);
			},logAndCry);
		} else {
			CryptoApi.getMarketPrice(mkt).then(function(obj) {
				res.json(_.pick(obj,q));
			},logAndCry);
		}
	} else {
		CryptoApi.getMarketPrice(mkt).then(function(obj) {
			res.json(obj);
		},logAndCry);
	}
})
.get('/api/v1/markets/:mkt/history/?', function(req,res,next) {
	var mkt = req.params.mkt,
		q = q ? req.query.q.split(',') : null,
		logAndCry = function(err) {
			console.log('ooooh noooo, an error in our api!!!!', err);
			res.status(500).send('we encountered an error, we will do all we can');
		};

	if(q) {
		if(q.length===1) {
			CryptoApi.getHistoricalPrice(mkt,q[0]).then(function(obj) {
				var result = {};
				result[q[0]] = obj;
				res.json(result);
			},logAndCry);
		} else {
			CryptoApi.getHistoricalPrice(mkt).then(function(obj) {
				res.json(_.pick(obj,q));
			},logAndCry);
		}
	} else {
		CryptoApi.getHistoricalPrice(mkt).then(function(obj) {
			res.json(obj);
		},logAndCry);
	}
})
.all('*', function(req,res,next) {
	var err = res.locals.error;

	if(!err) {
		return next();
	}

	console.log(err);
	
	if(req.xhr) {
		res.json({ err: err });
	} else {
		res.send(err);
	}
});

// Create server and start listening
http.createServer(app).listen(port, function(err) {
	if(err) {
		return console.log(err);
	}
	console.log('Listening on port ' + port);
});
