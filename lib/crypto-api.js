// Crypto-Api Module

var Q = require('q'),
	_ = require('lodash-node'),
	Bitstamp = require('bitstamp'),
	BTCE = require('btce'),
	MtGox = require('mtgox'),
	Coinbase = require('coinbase'),
	Cryptsy = require('cryptsy');

module.exports = {
	'sources': {
		'bitstamp': {
			'api': new Bitstamp(),
			'price': function(mkt) {
				var _this = this,
					r = Q.defer();

				this.api.ticker(function(err,data) {
					if(err) {
						return r.reject(err);
					}

					_this.lastData = data;
					_this.lastCall = Date.now();

					r.resolve(data.last);
				});
				return r.promise;
			},
			'getLast': function(mkt,dat) {
				switch(dat) {
					case 'price':
						return this.lastData.last;
						break;
					default:
						return this.lastData;
				}
			},
			'ratepersec': .5,
			'lastCall': null,
			'lastData': {}
		},
		'btce': {
			'api': new BTCE(),
			'price': function(mkt) {
				var _this = this,
					r = Q.defer();

				this.api.ticker({ pair:mkt },function(err,data) {
					if(err) {
						return r.reject(err);
					}

					_this.lastData[mkt] = data;
					_this.lastCall = Date.now();

					r.resolve(data.ticker.last);
				});
				return r.promise;
			},
			'getLast': function(mkt,dat) {
				switch(dat) {
					case 'price':
						return this.lastData[mkt].ticker.last;
						break;
					default:
						return this.lastData;
				}
			},
			'ratepersec': .5,
			'lastCall': null,
			'lastData': {}
		},
		'mtgox': {
			'api': new MtGox(),
			'price': function(mkt) {
				var _this = this,
					r = Q.defer(),
					mkt = mkt.replace('_','').toUpperCase();

				this.api.market(mkt,function(err,data) {
					if(err) {
						return r.reject(err);
					}

					_this.lastData[mkt] = data;
					_this.lastCall = Date.now();

					r.resolve(data.last);
				});
				return r.promise;
			},
			'getLast': function(mkt,dat) {
				switch(dat) {
					case 'price':
						return this.lastData[mkt].last;
						break;
					default:
						return this.lastData;
				}
			},
			'ratepersec': .5,
			'lastCall': null,
			'lastData': {}
		},
		'coinbase': {
			'api': new Coinbase({ APIKey: 'f1d5c79049a3b0d8f42ed8ab9a82793221b2e448e23eefd6613fe0ad8d586529' }),
			'price': function(mkt) {
				var _this = this,
					r = Q.defer(),
					mkt = mkt.replace('_','').toUpperCase();

				this.api.prices.spot_rate(function (err, data) {
					if(err) {
						return r.reject(err);
					}

					_this.lastData = data;
					_this.lastCall = Date.now();

					r.resolve(data.amount);
				});

				return r.promise;
			},
			'getLast': function(mkt,dat) {
				switch(dat) {
					case 'price':
						return this.lastData.btc_to_usd;
						break;
					default:
						return this.lastData;
				}
			},
			'history': function() {
				var _this = this,
					r = Q.defer();

				this.api.prices.historical({ convert: 'array', parse:true }, function(err, data) {
					if(err) {
						console.log('coinbase history error',err);
						return r.reject(err);
					}

					r.resolve(data);
				});

				return r.promise;
			},
			'ratepersec': .5,
			'lastCall': null,
			'lastData': null
		},
		'cryptsy': {
			'api': new Cryptsy(),
			'ratepersec': .5,
			'lastCall': null,
			'lastData': null
		}
	},
	// Get market price, optionally pass an exchange (source)
	// returns promise for object of market all market prices
	// queries should check the last time they were run and use cached data if not stale enough
	// Right now we're caching data at the local level, but we should move the actual data to mongo, maybe keep the application clock check?
	// this can all be tweaked, but we want to stay below thresholds of api policies
	'getMarketPrice': function(mkt,src) {
		var _this = this,
			result = Q.defer(),
			_result = { src: src },
			source;

		// Check if we have a market, we need one
		if(mkt) {
			// check if there is a source, if so, we get a 
			if(src) {
				source = _this.sources[src];
				// check if we have made a call within the rate limit, if not we make a new call and store the data
				if(!!source.lastCall || (Date.now() - source.lastCall > 1000/source.ratepersec)) {
					// We make a price call now from our source
					if(!source.price) {
						_result.price = 'coming soon...';
						result.resolve(_result);
					} else {
						source.price(mkt).then(function(price) {
							_result[mkt] = price;
							result.resolve(_result);
						}, result.reject);
					}
				} else {
					// Send back cached data since we got data not that long ago
					_result[mkt] = source.getLast(mkt,'price');
					result.resolve(_result);
				}
			} else {
				// No source, which means we send back a whole object or something, right?
				Q.all(_.map(_this.sources, function(src,name) {
					// recursiveness
					return _this.getMarketPrice(mkt,name);
				})).then(function(dataArr) {
					// resolving the array of promises from the recursive function we can map them to the names by assuming the order is maintained in sources
					var names = _.keys(_this.sources),
						dataObj = {};
					_.each(dataArr, function(src,i) {
						dataObj[names[i]] = src;
					});
					result.resolve(dataObj);
				}, function(err) {
					console.log('we have made a huge mistake', err);
					result.reject('we have made a huge mistake')
				});
			}
		} else {
			result.reject('mkt is undefined');
		}

		return result.promise;
	},
	'getHistoricalPrice': function(mkt,src) {
		var _this = this,
			mkt = mkt || 'btc_usd',
			src = src || 'coinbase',
			result = Q.defer();  // starting with coinbase, dont even need mkt cause there is only usd

		if(src!=='coinbase') {
			return result.reject(src+' API not supported yet');
		}

		result.resolve(this.sources[src].history());
		return result.promise;
	}
};
